public class Ejercicio4App {
	public static void main(String[] args) {
		int n = 50;
		
		System.out.println("Valor inicial de N = " + n);
		
		//Incrementar N en 77
		n += 77;
		System.out.println("N + 77 = " + n);
		
		//Decrementarla en 3
		n -= 3;
		System.out.println("N - 3 = " + n);
		
		//Duplicar su valor
		n *= 2;
		System.out.println("N * 2 = " + n);
	}
}